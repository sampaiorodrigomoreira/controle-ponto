package br.com.demo.controleponto.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class Usuario {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idUsuario;

    @NotBlank
    @NotNull
    private String nome;

    @CPF
    private String cpf;

    @Email
    @NotBlank
    @NotNull
    private String email;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dataDeCadastro;

    public Usuario() {}

    public Usuario(int id, @NotBlank @NotNull String nome, @CPF String cpf, @Email @NotBlank @NotNull String email,
                   LocalDate dataDeCadastro) {
        this.idUsuario = id;
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.dataDeCadastro = dataDeCadastro;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataDeCadastro() {
        return dataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        this.dataDeCadastro = dataDeCadastro;
    }
}

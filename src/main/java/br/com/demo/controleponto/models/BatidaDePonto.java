package br.com.demo.controleponto.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.demo.controleponto.enums.TipoBatidaEnum;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class BatidaDePonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idBatidaPonto;

    @NotBlank
    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dataHoraBatida;

    private TipoBatidaEnum tipoBatida;

    public BatidaDePonto() {}

    public BatidaDePonto(int idBatidaPonto, Usuario usuario, LocalDate dataHoraBatida,
                         TipoBatidaEnum tipoBatida) {
        this.idBatidaPonto = idBatidaPonto;
        this.usuario = usuario;
        this.dataHoraBatida = dataHoraBatida;
        this.tipoBatida = tipoBatida;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getIdBatidaPonto() {
        return idBatidaPonto;
    }

    public void setIdBatidaPonto(int idBatidaPonto) {
        this.idBatidaPonto = idBatidaPonto;
    }

    public LocalDate getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDate dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}

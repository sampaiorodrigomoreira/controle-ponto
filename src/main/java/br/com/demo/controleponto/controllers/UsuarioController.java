package br.com.demo.controleponto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import br.com.demo.controleponto.models.Usuario;
import br.com.demo.controleponto.services.UsuarioService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    public Iterable<Usuario> buscarTodosUsuarios(){
        Iterable<Usuario> usuariosCadastrados = usuarioService.buscarTodosUsuarios();
        return usuariosCadastrados;
    }


    @GetMapping("/{id}")
    public Usuario buscarUsuarioPoId(@PathVariable(name = "id") int id){
        Optional<Usuario> usuario = Optional.ofNullable(usuarioService.buscarUsuarioPorId(id));

        if(usuario.isPresent()){
            return usuario.get();
        }
        throw new  RuntimeException("Usuário não encontrado");
    }

    @PostMapping
    public Usuario cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioCadasrtado = usuarioService.cadastrarUsuario(usuario);
        return usuarioCadasrtado;
    }

    @PutMapping("/{id}")
    public Usuario atualizarUsuario(@PathVariable(name = "id") int id, @RequestBody Usuario usuario){
        try{
            Usuario usuarioAtualizado = usuarioService.atualizarDadosUsuario(id, usuario);
            return usuarioAtualizado;

        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}

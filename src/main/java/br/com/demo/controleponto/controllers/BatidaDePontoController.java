package br.com.demo.controleponto.controllers;

import br.com.demo.controleponto.enums.TipoBatidaEnum;
import br.com.demo.controleponto.models.BatidaDePonto;
import br.com.demo.controleponto.services.BatidaDePontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ponto")
public class BatidaDePontoController {

    @Autowired
    private BatidaDePontoService batidaDePontoService;

    @PostMapping("/{id_usuario}")
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaDePonto registrarPonto(@PathVariable(name = "id_usuario") int id, @RequestBody BatidaDePonto batidaDePonto){

        BatidaDePonto batidaRegistrada = batidaDePontoService.criarBatidaDePonto(batidaDePonto);
        return batidaRegistrada;
    }
}

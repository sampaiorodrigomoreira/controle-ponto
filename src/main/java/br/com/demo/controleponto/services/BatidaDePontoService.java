package br.com.demo.controleponto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.demo.controleponto.models.BatidaDePonto;
import br.com.demo.controleponto.models.Usuario;
import br.com.demo.controleponto.repositories.BatidaDePontoRepository;
import br.com.demo.controleponto.repositories.UsuarioRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class BatidaDePontoService {

    @Autowired
    private BatidaDePontoRepository batidaDePontoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public BatidaDePonto criarBatidaDePonto(BatidaDePonto batidaDePonto){
        Usuario usuario = usuarioRepository.findById(batidaDePonto.getUsuario().getIdUsuario());

        if(usuarioRepository.existsById(usuario.getIdUsuario())){
            BatidaDePonto novaBatidaDePonto = new BatidaDePonto();
            novaBatidaDePonto.setUsuario(usuario);
            novaBatidaDePonto.setDataHoraBatida(batidaDePonto.getDataHoraBatida());
            novaBatidaDePonto.setTipoBatida(batidaDePonto.getTipoBatida());
            batidaDePontoRepository.save(novaBatidaDePonto);

            return novaBatidaDePonto;
        }else
            throw new RuntimeException("Usuario não encontrado");
    }

//    public Iterable<Usuario> buscarTodosPorTipoDeBatida(TipoBatidaEnum tipoBatidaEnum){
//        Iterable<Usuario> usuarios = batidaPontoRepository.findAllByTipoBatidaDePonto(tipoBatidaEnum);
//        return usuarios;
//    }


}

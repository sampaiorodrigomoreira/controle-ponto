package br.com.demo.controleponto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.demo.controleponto.models.Usuario;
import br.com.demo.controleponto.repositories.UsuarioRepository;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario cadastrarUsuario (Usuario usuario){
        Usuario usuarioCadatrado = usuarioRepository.save(usuario);
        return usuarioCadatrado;
    }

    public Usuario buscarUsuarioPorId (int id){
        if(usuarioRepository.existsById(id)){

            Usuario dadosUsuario = usuarioRepository.findById(id);
            return dadosUsuario;
        }
        throw new  RuntimeException("Usuario não encontrado");
    }

    public Iterable<Usuario> buscarTodosUsuarios(){
        Iterable<Usuario> usuariosCadastrados = usuarioRepository.findAll();
        return usuariosCadastrados;
    }

    public Usuario atualizarDadosUsuario(int id, Usuario usuario){
        if(usuarioRepository.existsById(id)){
            usuario.setIdUsuario(id);
            Usuario usuarioAtualizado = cadastrarUsuario(usuario);
            return usuarioAtualizado;
        }
        throw new  RuntimeException("Usuario não encontrado");
    }
}

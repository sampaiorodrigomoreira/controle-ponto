package br.com.demo.controleponto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.demo.controleponto.models.BatidaDePonto;

public interface BatidaDePontoRepository extends CrudRepository<BatidaDePonto, Integer> {
}
